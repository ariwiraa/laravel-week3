<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;

class GenreController extends Controller
{

    public function index(){
        $genre = Genre::all();
        return view('adminlte.genre.index', compact('genre'));
    }
    
}
