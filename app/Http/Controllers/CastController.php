<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use DB;
use App\Cast;

class CastController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['index']);
    }

    public function create(){
        return view('adminlte.cast.create');
    }
    
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        // dd($request->all());
        /*$query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);*/

        //menggunakan eloquent
        Cast::create([
    		'nama' => $request->nama,
    		'umur' => $request->umur,
            'bio' => $request->bio
    	]);

        Alert::success('Berhasil', 'Berhasil menambah post');
        //->with('success', 'Data berhasil ditambahkan')
        return redirect('/cast');
    }

    public function index(){
        // $cast = DB::table('cast')->get();
        //Ambil data yang sudah di post berdasarkan users_id
        // $user = Auth::user();
        // $cast = $user->cast;
        // dd('cast');
        $cast = Cast::all();
        return view('adminlte.cast.index', compact('cast'));
    }

    public function show($cast_id){
        // $cast = DB::table('cast')->where('id', $cast_id)->first();
        $cast = Cast::find($cast_id);
        return view('adminlte.cast.show', compact('cast'));
    }

    public function edit($cast_id)
    {
        // $cast = DB::table('cast')->where('id', $cast_id)->first();
        $cast = Cast::find($cast_id);
        return view('adminlte.cast.edit', compact('cast'));
    }

    public function update($cast_id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // $query = DB::table('cast')
        //     ->where('id', $cast_id)
        //     ->update([
        //         'nama' => $request["nama"],
        //         'umur' => $request["umur"],
        //         'bio' => $request["bio"]
        //     ]);
        $cast = Cast::find($cast_id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();
        return redirect('/cast');
    }

    public function destroy($cast_id)
    {
        // $query = DB::table('cast')->where('id', $cast_id)->delete();
        $cast = Cast::find($cast_id);
        $cast->delete();
        return redirect('/cast');
    }

}
