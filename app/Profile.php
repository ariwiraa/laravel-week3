<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
  protected $table = "profile";

    public function users(){
        return $this->belongsTo('App\User');
      }
}
