<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = "film";

    public function genre()
  {
   return $this->belongsTo('App\Genre', 'genre_id');
  }
}
