<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    protected $table = "kritik";

    public function users(){
        return $this->belongsTo('App\User', 'users_id');
    }
}
