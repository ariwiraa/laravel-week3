<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="firstName"> <br><br> 
        
        <label>Last name:</label> <br><br>
        <input type="text" name="lastName"><br><br>

        <label>Gender:</label> <br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br><br>

        <label>Nationality:</label> <br><br>
        <select name="gender" >
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
        </select> <br><br>
        
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="languageSpoken">Indonesia <br>
        <input type="checkbox" name="languageSpoken">English <br>
        <input type="checkbox" name="languageSpoken">Other <br><br>

        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>

        <button>Sign Up</button>
    </form>
</body>
</html>