@extends('adminlte.master')

@section('content')
<div class="mx-3 mt-3">
    <div class="card card-primary"> 
        <div class="card-header">
            <h3>One to Many</h3>
        </div>
            <table class="table table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">kategori</th>
                        <th scope="col">title</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($genre as $value)
                    <tr>
                        <td>{{$value->name}}</td>
                        <td>
                        @foreach($value->genre as $g)
                        {{$b->name}}
                        @endforeach
                        </td>
                    </tr>
                    @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>  
                    @endforelse
                <tbody>
            </table>
    </div>
</div>

                
@endsection