@extends('adminlte.master')

@section('content')
<div class="mx-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">    
            <h3>One to one</h3>
        </div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">username</th>
                        <th scope="col">email</th>
                        <th scope="col">alamat</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($user as $value)
                    <tr>
                        <td>{{$value->username}}</td>
                        <td>{{$value->email}}</td>
                        <td>{{$value->profile->alamat}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection