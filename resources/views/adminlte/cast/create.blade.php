@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3"> 
    <div class="card card-primary">
        <div class="card-header">
            
            <h2 class="card-title">Tambah Data</h2>
        </div>
            <form action="/cast" method="POST">
                @csrf
                <div class="card-body">

                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama', '') }}" placeholder="Masukan Nama">
                        @error('nama')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="text" class="form-control" name="umur" id="umur" value="{{ old('umur', '') }}" placeholder="Masukkan Umur">
                        @error('umur')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <textarea class="form-control" name="bio" id="bio" placeholder="Masukkan Bio" value="{{ old('bio', '') }}" cols="30" rows="10"></textarea>
                        @error('bio')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary ml-3 mb-3">Tambah</button>
            </form>
    </div>
</div>
    
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/f3tkb7fbup72qwyy0913266pijll4cysfgwon5gbw4o3kbfb/tinymce/5/tinymce.min.js"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
   });
  </script>
  
@endpush