@extends('adminlte.master')

@section('content')

<div class="ml-3 mt-3">
    <div class="card card-primary"> 
        <div class="card-header">
            <h2 class="card-title">Tambah Data</h2>
        </div>
        <!--end card header-->

        <div class="card-body">
            {{-- @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif --}}
            
            <a href="/cast/create" class="btn btn-primary my-2">Tambah</a>
            <table class="table table-bordered ">
                <thead class="thead-light">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Umur</th>
                    <th scope="col">Bio</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</th>
                            <td>{{$value->nama}}</td>
                            <td>{{$value->umur}}</td>
                            <td>{!!$value->bio!!}</td>
                            <td>
                                <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                                <form action="/cast/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1 btn-sm" value="Delete">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>  
                        @endforelse              
                    </tbody>
                </table>
            </div>
        
        </div>
</div>
@endsection
