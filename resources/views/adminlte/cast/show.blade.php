@extends('adminlte.master')

@section('content')

<div class="mx-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
            <h2>Show Cast {{$cast->id}}</h2>
        </div>
        <div class="card-body">
            <h4>{{$cast->nama}} ({{$cast->umur}})</h4>
            <p>{{$cast->bio}}</p>
        </div>
    </div>
</div>
@endsection