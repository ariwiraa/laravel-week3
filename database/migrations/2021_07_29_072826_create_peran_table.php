<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peran', function (Blueprint $table) {
            $table->bigIncrements('id');
            //Foreign Key
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film');
            // end Foreign Key

            //Foreign Key
            $table->unsignedBigInteger('cast_id');
            $table->foreign('cast_id')->references('id')->on('cast');
            // end Foreign Key
            
            $table->string('nama', 45);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peran');
    }
}
