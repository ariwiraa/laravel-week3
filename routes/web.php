<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

//Admin Lte
Route::get('/table', function(){
    return view('items.table');
});

Route::get('/data-table', function(){
    return view('items.data-table');
});
//end admin lte

//CRUD
// Route::get('/cast/create', 'CastController@create');
// Route::get('/cast', 'CastController@index');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast/{cast_id}', 'CastController@show');
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');
// Route::delete('/cast/{cast_id}', 'CastController@destroy');
// //end CRUD

Route::resource('cast', 'CastController');
Route::resource('film', 'FilmController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user', 'UserController@index');
Route::get('/genre', 'GenreController@index');